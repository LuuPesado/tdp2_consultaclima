import unittest
from api.firebase_messages import FirebaseMessagesSender
from decouple import config


class TestFirebaseMessagesSender(unittest.TestCase):
    def test_get_buenos_aires_wheather_info(self):
        fms = FirebaseMessagesSender()
        response = fms.get()
        print(response)
        self.assertEqual(response.split('/')[1], 'magios-weather')
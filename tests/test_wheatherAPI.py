import unittest
from api.weather_api import OpenWheatherAPI


class TestOpenWheatherAPI(unittest.TestCase):
    def test_get_buenos_aires_wheather_info(self):
        ow = OpenWheatherAPI()
        response = ow.get(city_name='Buenos Aires')
        print(response)
        self.assertEqual(response['status'], 200)

    def test_get_nueva_york_wheather_info(self):
        ow = OpenWheatherAPI()
        response = ow.get(city_name='Nueva York')
        print(response)
        self.assertEqual(response['status'], 200)

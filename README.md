# tdp2_consultaClima
=======================


[![pipeline status](https://gitlab.com/LuuPesado/tdp2_consultaclima/badges/master/pipeline.svg)](https://gitlab.com/LuuPesado/tdp2_consultaclima/commits/master)



## Introducción
Proyecto creado para el tp inicial de la materia Taller de Desarrollo de proyectos 2 . FIUBA

El enunciado puede encontrarse [acá](https://docs.google.com/document/d/19A2AYNuQnfhNXRqYK-lQbsUNCEfd69c0ss1LuxesqIU/edit)

## Instalación Local

 ### Requisitos
  - Python >= 3.6.X (recomendamos utilizar [virtualenv](https://medium.com/@aaditya.chhabra/virtualenv-with-virtualenvwrapper-on-ubuntu-34850ab9e765))

 ### Python

 Para instalar el código y sus dependencias

 ```
 pip install -r requirements.txt
 or  
 pip3 install -r requirements.txt
 ```


## Ejecucion

Para ejecutarlo localmente existe una entrada de `Makefile`

```make run```

o en su defecto

```gunicorn app:app```

Esto localmente iniciará un servidor escuchando en 

`http://localhost:8000`

## .env

Se necesita crear un archivo .env con las siguientes variables de ambiente:

```
API_KEY = "asdf12345" # Reemplazar con el api-key correspondiente
FIREBASE_SERVER_KEY = ...
TEST_DEVICE_TOKEN = ...
PORCENTAJE_LLUVIA = 5
```

## CICD

Para asegurate que no vaya a fallar el pipeline, deberas correr los tests y flask8 por consola antes de commitear

```
flake8 --max-line-length=120 *.py
pytest
```

Cada vez que se haga un push a master, se hara un deploy de la app a heroku: https://magios-weather-server.herokuapp.com/


## Endpoints

/api/weather/<string:city_name>: Devuelve un diccionario con el clima de la ciudad que se pasa como city_name. Por ejemplo: /api/weather/Buenos%20Aires devuelve los datos de buenos aires




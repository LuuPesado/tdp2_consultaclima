 
run:
	gunicorn app:app
flake_test:
	flake8 . --count --statistics

.PHONY: run, flake_test
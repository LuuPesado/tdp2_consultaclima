from flask import Flask
from flask_restful import Api
from flask_cors import CORS
from api.weather_api import OpenWheatherAPI
from api.firebase_messages import FirebaseMessagesSender

app = Flask(__name__)
CORS(app)
api = Api(app)

api.add_resource(OpenWheatherAPI, '/api/weather/<string:city_name>')
api.add_resource(FirebaseMessagesSender, '/api/firebase/<string:device_token>')

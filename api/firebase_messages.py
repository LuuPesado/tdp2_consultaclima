from flask_restful import Resource
import requests
import json
from decouple import config
import firebase_admin
from firebase_admin import messaging, credentials


class FirebaseMessagesSender(Resource):
    def get(self):
        cred = credentials.Certificate("magios-weather-firebase-adminsdk-s6bej-741820ba98.json")
        firebase_admin.initialize_app(cred)
        topic = 'weather'
        message = messaging.Message(
            notification=messaging.Notification(
                title='Magios Weather App',
                body=config('PORCENTAJE_LLUVIA'),
            ),
            topic=topic
        )
        response = messaging.send(message)
        print('Successfully sent message: ', response)
        return response

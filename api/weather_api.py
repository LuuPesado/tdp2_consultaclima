from flask_restful import Resource
import requests
import json
from decouple import config

BASE_URL = "https://api.openweathermap.org/data/2.5/forecast?"


class OpenWheatherAPI(Resource):
    def get(self, city_name):
        try:
            api_key = config('API_KEY')
            url = BASE_URL + "q=%s&appid=%s&units=metric&lang=es" % (city_name, api_key)
            print(url)
            response = requests.get(url)
            data = response.json()
            threeHourForecast = data['list'][0]
            print(threeHourForecast)
            return {'data': {
                'rainChance': threeHourForecast['pop'],
                'temperature': threeHourForecast['main']['temp'],
                'weather': threeHourForecast['weather'][0]['main']
            }, 'status': 200}
        except Exception as e:
            return e
